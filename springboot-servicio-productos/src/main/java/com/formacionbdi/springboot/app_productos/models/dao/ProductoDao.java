package com.formacionbdi.springboot.app_productos.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.formacionbdi.springboot.app_productos.models.entity.Producto;

public interface ProductoDao extends CrudRepository<Producto, Long>{
	
}
