package com.formacionbdi.springboot.app_productos.models.service;

import java.util.List;

import com.formacionbdi.springboot.app_productos.models.entity.Producto;

public interface IProductoService {
	
	public List<Producto> findAll();
	public Producto findById(Long id);
}
